import { Chip, FormControl, IconButton, ListItem, ListSubheader, Menu, MenuItem, Select, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';
import { columns_verbose, lookups, lookups_verbose } from './helpers';
import SearchIcon from '@mui/icons-material/Search';
import { ColumnString } from './helpers'

type filtersType = {
    field: string;
    value: string;
    lookup: string;
}

interface FilterType {
    [key: string]: any;
    filters: filtersType[];
    setFilters: any;
    contextMenuField: string;
    contextMenu: contextMnueType | null;
    handleClose: any;
}

export type contextMnueType = {
    mouseY: number;
    mouseX: number;
}


export default function FilterMenu({ filters, setFilters, contextMenuField, handleClose, contextMenu }: FilterType) {
    const [currentLookup, setCurrentLookup] = useState("");
    const [currentValue, setCurrentValue] = useState("");

    const menuClose = () => {
        handleClose();
        setCurrentLookup("");
        setCurrentValue("");
    }

    const handleDelete = (field: string) => {
        setFilters(filters.filter(
            el => el["field"] !== field
        ))
    }

    const onKeyUp = (event: any) => {
        if (event.key === "Enter") {
            handleNewFilter();
        }
    }

    const handleNewFilter = () => {
        if (currentValue !== "") {
            setFilters([
                { "field": contextMenuField, "value": currentValue.toLocaleLowerCase(), "lookup": currentLookup },
                ...filters.filter(
                    el => el["field"] !== contextMenuField
                )
            ])
        } else if (currentLookup === "[empty]" || currentLookup === "[not empty]") {
            setFilters([
                { "field": contextMenuField, "value": currentValue.toLocaleLowerCase(), "lookup": currentLookup },
                ...filters.filter(
                    el => el["field"] !== contextMenuField
                )
            ])
        }

        menuClose();
    }


    return (
        <Menu
            open={contextMenu !== null}
            onClose={menuClose}
            disableAutoFocusItem
            variant="menu"
            anchorReference="anchorPosition"
            anchorPosition={
                contextMenu !== null
                    ? { top: contextMenu.mouseY, left: contextMenu.mouseX }
                    : undefined
            }
            MenuListProps={{
                subheader: (
                    <ListSubheader component="div">
                        {columns_verbose[contextMenuField as keyof ColumnString]}
                    </ListSubheader>
                )

            }}
            PaperProps={{
                style: {
                    borderRadius: 0,
                    border: '1px solid #e7e7e7',
                    maxHeight: 242.56
                }
            }}
        >
            <ListItem>
                <FormControl size="small" variant="standard" sx={{ mr: 1 }}>
                    <Select
                        value={currentLookup}
                        onChange={e => setCurrentLookup(e.target.value)}
                        MenuProps={{
                            PaperProps: {
                                elevation: 4,
                                style: {
                                    borderRadius: 0,
                                    border: '1px solid #e7e7e7',
                                    maxHeight: (32.58 * 5) + 8
                                }
                            }
                        }}
                    >
                        <MenuItem value="">Contains</MenuItem>
                        {lookups.map(lookup => <MenuItem key={`lookups-dropdown--${lookup}`} value={lookup}>{lookup}</MenuItem>)}
                    </Select>
                </FormControl>
                <TextField value={currentValue} onKeyUp={onKeyUp} onChange={e => setCurrentValue(e.target.value)} size="small" variant="standard" placeholder="filter" />
                <IconButton size="small" onClick={handleNewFilter}>
                    <SearchIcon />
                </IconButton>
            </ListItem>
            {filters.length ?
                <div>
                    <ListSubheader component="div" sx={{ pt: 3 }}>
                        <Typography sx={{size:"small", fontSize:12}} >
                            <i>Current Filters:</i>
                        </Typography>
                    </ListSubheader>
                    {filters.map(el => {
                        return (
                            <ListItem key={`${el["field"]}--filter`} dense>
                                <Chip
                                    size="small"
                                    style={{ marginLeft: 5 }}
                                    variant="outlined"
                                    color="secondary"
                                    label={`${columns_verbose[el["field"]]} ${lookups_verbose[el["lookup"]]} ${el["value"]}`}
                                    onDelete={() => handleDelete(el["field"])}
                                />
                            </ListItem>
                        )
                    })}
                </div>
                :
                null
            }
        </Menu>
    )
}