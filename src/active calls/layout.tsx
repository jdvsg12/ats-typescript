import React from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import ActiveCalls from "./ActiveCalls";


export const Ariatheme = createTheme({
  palette: {

    // type: "light",

    primary: {
      light: '#69caff',
      main: "#169aed",
      dark: '#006cba',
      contrastText: "#fff"
    },
    secondary: {
      light: '#7261ff',
      main: "#2134ED",
      dark: '#0007b9',
      contrastText: "#fff"
    },
    error: {
      light: '#ff6859',
      main: "#ed2d2e",
      dark: '#b20005',
      contrastText: "#fff"
    },
    warning: {
      light: '#ff8d4f',
      main: "#ED5C21",
      dark: '#b32900',
      contrastText: "#fff"
    },
    info: {
      light: '#6bfffd',
      main: "#09edca",
      dark: '#00ba99',
      contrastText: "#000"
    },
    success: {
      light: '#8feb8b',
      main: "#5cb85c",
      dark: '#26872f',
      contrastText: "#000"
    },
    background: {
      default: "#eeeeee",
    },
  },
  typography: {
    // In Chinese and Japanese the characters are usually larger,
    // so a smaller fontsize may be appropriate.
    fontSize: 12,
  }  
});

export default function Layout() {
  return (
    <ThemeProvider theme={Ariatheme}>
      <CssBaseline />
      <Box sx={{ display: "flex" }}>
        <ActiveCalls />
      </Box>
    </ThemeProvider>
  );
}
