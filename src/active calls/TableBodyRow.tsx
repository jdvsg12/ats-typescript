import React from 'react';
import { Box } from '@mui/system';
import { columns_width, convertHHMMSS, isValidJSON, acRowSize, columns } from './helpers';
import { TableBodyCell } from './TableBodyCell';
import { Skeleton } from '@mui/material';


type DataObjectActiveCalls = {
    id: string;
    uuid: string;
    country: string;
    porta: string;
    media: string;
    state: string;
    call_start: string;
    call_answered: string;
    billsec: string | number;
    src_number: string;
    dst_number: string;
    area_code: string;
    customer: string;
    routing_by: string;
    dst_prefix: string;
    codecs: string;
    dst_transformed: string;
    ip_real: string;
    ip_caller: string;
    ip_called: string;
    ip_switch: string;
    src_rol: string;
    src_rol_color: string;
    dst_rol: string;
    dst_rol_color: string;
    rol_data: string;
    rol_data_yesterday: string;
    park_timeout: string;
    park_ininit: string;
    rate: string;
    pai: string;
    rpid: string;
    did_transformation: string;
    cid_name: string;
    cid_number: string;
    cl_rate: string;
    pr_billed: number;
    cl_billed: number;
    profit: number;
    provider: string;
    park_time: number;
    prefix_acl: string;
    initial_cid_name: string;
    initial_cid_number: string;
    initial_pai: string;
    initial_rpid: string;
};
type field = {
    [key: string]: any;
    field: string;
};

const TableBodyRow = React.memo<DataObjectActiveCalls>(
    (props) => {
        return (
            <Box component="div" sx={{
                display: 'flex',
                flexWrap: 'nowrap',
                width: '100%',
                maxHeight: acRowSize,
                bgcolor: props.dst_rol_color,
                borderBottom: '1px solid #707070',
                '&:hover': { bgcolor: 'black' },
                '&:hover div': { color: 'white' },
                '&:hover span[data-context="src_rol"]': { color: 'black' },
                fontSize: 11
            }}>

                <TableBodyCell field="country" width={1 + columns_width["country"]} value={props.country} />
                <TableBodyCell field="area_code" width={1 + columns_width["area_code"]} value={props.area_code} />
                <TableBodyCell field="porta" width={1 + columns_width["porta"]} value={props.porta} />
                <TableBodyCell field="src_number" width={1 + columns_width["src_number"]} value={props.src_number} />
                <TableBodyCell field="dst_number" width={1 + columns_width["dst_number"]} value={props.dst_number} />
                <TableBodyCell field="call_start" width={1 + columns_width["call_start"]} value={new Date(parseInt(props.call_start) / 1000).toLocaleTimeString("en-US")} />
                <TableBodyCell field="call_answered" width={1 + columns_width["call_answered"]} value={props.call_answered !== "0" ? new Date(parseInt(props.call_answered) / 1000).toLocaleTimeString("en-US") : "---"} />
                <TableBodyCell field="state" width={1 + columns_width["state"]} value={props.state} />

                <Box component="div" data-context="billsec" sx={{ whiteSpace: 'nowrap', width: (1 + columns_width["billsec"]), mx: 2 }}>
                    <span data-context="billsec">
                        {props.billsec > 0 ?
                            <span data-context="billsec" style={{ background: '#7261ff', padding: 3, borderRadius: 2, color: 'white' }}>{convertHHMMSS(props.billsec)}</span>
                            : '...'
                        }
                    </span>
                </Box>

                <TableBodyCell field="media" width={1 + columns_width["media"]} value={props.media} />
                <TableBodyCell field="customer" width={1 + columns_width["customer"]} value={props.customer} />
                <TableBodyCell field="prefix_acl" width={1 + columns_width["prefix_acl"]} value={props.prefix_acl} />
                <TableBodyCell field="provider" width={1 + columns_width["provider"]} value={props.provider} />
                <TableBodyCell field="dst_prefix" width={1 + columns_width["dst_prefix"]} value={props.dst_prefix} />
                <TableBodyCell field="dst_transformed" width={1 + columns_width["dst_transformed"]} value={props.dst_transformed} />
                <TableBodyCell field="rate" width={1 + columns_width["rate"]} value={props.rate} />
                <TableBodyCell field="pr_billed" width={1 + columns_width["pr_billed"]} value={props.pr_billed.toFixed(4)} />
                <TableBodyCell field="cl_rate" width={1 + columns_width["cl_rate"]} value={props.cl_rate} />
                <TableBodyCell field="cl_billed" width={1 + columns_width["cl_billed"]} value={props.cl_billed.toFixed(4)} />
                <TableBodyCell field="profit" width={1 + columns_width["profit"]} value={props.profit.toFixed(4)} />
                <TableBodyCell field="ip_real" width={1 + columns_width["ip_real"]} value={props.ip_real} />
                <TableBodyCell field="ip_caller" width={1 + columns_width["ip_caller"]} value={props.ip_caller} />
                <TableBodyCell field="ip_called" width={1 + columns_width["ip_called"]} value={props.ip_called} />
                <TableBodyCell field="ip_switch" width={1 + columns_width["ip_switch"]} value={props.ip_switch} />
                <TableBodyCell field="routing_by" width={1 + columns_width["routing_by"]} value={props.routing_by.split(" ").slice(-1)[0].toUpperCase()} />
                <TableBodyCell field="src_rol" width={1 + columns_width["src_rol"]} bgcolor={props.src_rol_color} value={props.src_rol} />
                <TableBodyCell field="dst_rol" width={1 + columns_width["dst_rol"]} value={props.dst_rol} />
                <TableBodyCell field="cause_sip" width={1 + columns_width["cause_sip"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).cause_sip.replace("sip:", "") : "New"} />
                <TableBodyCell field="q850" width={1 + columns_width["q850"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).q850 : "New"} />
                <TableBodyCell field="acd" width={1 + columns_width["acd"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.acd : "New"} />
                <TableBodyCell field="asr" width={1 + columns_width["asr"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.asr : "New"} />
                <TableBodyCell field="sec" width={1 + columns_width["sec"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.sec : "New"} />
                <TableBodyCell field="tat" width={1 + columns_width["tat"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.tat : "New"} />
                <TableBodyCell field="tct" width={1 + columns_width["tct"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.tct : "New"} />
                <TableBodyCell field="tft" width={1 + columns_width["tft"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.tft : "New"} />
                <TableBodyCell field="tst" width={1 + columns_width["tst"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.tst : "New"} />
                <TableBodyCell field="tcls" width={1 + columns_width["tcls"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.tcls : "New"} />
                <TableBodyCell field="t_calls" width={1 + columns_width["t_calls"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.t_calls : "New"} />
                <TableBodyCell field="t_fails" width={1 + columns_width["t_fails"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.t_fails : "New"} />
                <TableBodyCell field="t_answer" width={1 + columns_width["t_answer"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.t_answer : "New"} />
                <TableBodyCell field="t_fails_t" width={1 + columns_width["t_fails_t"]} value={isValidJSON(props.rol_data) ? JSON.parse(props.rol_data).stats.t_fails_t : "New"} />
                <TableBodyCell field="cause_sip_y" width={1 + columns_width["cause_sip_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).cause_sip.replace("sip:", "") : "---" : "---"} />
                <TableBodyCell field="q850_y" width={1 + columns_width["q850_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).q850 : "---" : "---"} />
                <TableBodyCell field="acd_y" width={1 + columns_width["acd_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.acd : "---" : "---"} />
                <TableBodyCell field="asr_y" width={1 + columns_width["asr_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.asr : "---" : "---"} />
                <TableBodyCell field="sec_y" width={1 + columns_width["sec_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.sec : "---" : "---"} />
                <TableBodyCell field="tat_y" width={1 + columns_width["tat_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.tat : "---" : "---"} />
                <TableBodyCell field="tct_y" width={1 + columns_width["tct_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.tct : "---" : "---"} />
                <TableBodyCell field="tft_y" width={1 + columns_width["tft_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.tft : "---" : "---"} />
                <TableBodyCell field="tst_y" width={1 + columns_width["tst_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.tst : "---" : "---"} />
                <TableBodyCell field="tcls_y" width={1 + columns_width["tcls_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.tcls : "---" : "---"} />
                <TableBodyCell field="t_calls_y" width={1 + columns_width["t_calls_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.t_calls : "---" : "---"} />
                <TableBodyCell field="t_fails_y" width={1 + columns_width["t_fails_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.t_fails : "---" : "---"} />
                <TableBodyCell field="t_answer_y" width={1 + columns_width["t_answer_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.t_answer : "---" : "---"} />
                <TableBodyCell field="t_fails_t_y" width={1 + columns_width["t_fails_t_y"]} value={isValidJSON(props.rol_data_yesterday) ? props.rol_data_yesterday !== "{}" ? JSON.parse(props.rol_data_yesterday).stats.t_fails_t : "---" : "---"} />
                <TableBodyCell field="park_timeout" width={1 + columns_width["park_timeout"]} value={props.park_timeout} />
                <TableBodyCell field="park_time" width={1 + columns_width["park_time"]} value={convertHHMMSS(props.park_time)} />
                <TableBodyCell field="park_ininit" width={1 + columns_width["park_ininit"]} value={props.park_ininit} />
                <TableBodyCell field="cid_name" width={1 + columns_width["cid_name"]} value={props.cid_name} />
                <TableBodyCell field="cid_number" width={1 + columns_width["cid_number"]} value={props.cid_number} />
                <TableBodyCell field="pai" width={1 + columns_width["pai"]} value={props.pai} />
                <TableBodyCell field="rpid" width={1 + columns_width["rpid"]} value={props.rpid} />
                <TableBodyCell field="did_transformation" width={1 + columns_width["did_transformation"]} value={props.did_transformation} />
                <TableBodyCell field="codecs" width={columns_width["codecs"]} value={props.codecs} />
            </Box>
        )
    }
)

export const LoaderRow = () => {
    return (
        <Box component="div" sx={{ display: 'flex', flexWrap: 'nowrap', width: '100%', height: acRowSize, bgcolor: 'white', borderBottom: '1px solid #eee' }}>

            {columns.map((col: field) => {
                return (
                    <Box
                        key={`skeleton--${col["field"]}`}
                        component="div"
                        sx={{ whiteSpace: 'nowrap', width: (1 + columns_width[col["field"]]), mx: 2 }}>

                        <Skeleton animation={false} height={10} sx={{ my: '3px' }} />
                    </Box>
                )
            })}
        </Box>
    )
}

export default TableBodyRow;