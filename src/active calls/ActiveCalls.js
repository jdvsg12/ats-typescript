import React, { useState, useEffect, useRef } from 'react';
import Paper from '@mui/material/Paper';
import { Box } from '@mui/system';
import TableBodyRow, { LoaderRow } from './TableBodyRow';
import { acRowSize, closestMultiple, filter_ac, getWindowDimensions } from './helpers';
import CachedIcon from '@mui/icons-material/Cached';
import MemoTableHeader from './TableHeader';
import FilterMenu from './FilterMenu';
import { Button, Divider, IconButton, Stack, Tooltip } from '@mui/material';
import FilterAltOffIcon from '@mui/icons-material/FilterAltOff';
import { isMobile } from 'react-device-detect';
import PlayCircleFilledIcon from '@mui/icons-material/PlayCircleFilled';
import PauseCircleIcon from '@mui/icons-material/PauseCircle';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Centrifuge from 'centrifuge';
import axios from 'axios';
// import { RowColumnData } from './helpers';
// import { contextMnueType } from './FilterMenu';

export default function ActiveCalls() {
    const [rows, setRows] = useState([]);
    const [oldBillsec, setOldBillsec] = useState([])
    const { height } = getWindowDimensions();
    const [tableScroll, setTableScroll] = useState(0);
    const [contextMenu, setContextMenu] = React.useState(null);
    const [contextMenuField, setContextMenuField] = useState("");
    const [filters, setFilters] = useState([]);
    const [errorWS, setErrorWS] = useState(false);
    const [pause, setPause] = useState(false);
    const newPause = useRef(false);
    const state = rows.map((Data) => Data.state)[0];
    const billsec = rows.map((Data) => Data.billsec)[0];
    const oldDataBillsec = oldBillsec.map((Data) => Data.billsec)[0];

    // type DataActiveCalls = RowColumnData[];


    if (rows.length <= 2 && rows.length >= 1) {

        switch (state) {
            case "DOWN":
                if (billsec === 0) {
                    setRows([])
                } else {
                    setRows([])
                };
                break;

            case "ACTIVE":
                if (billsec >= 0) {
                    const interval = setInterval(() => {
                        if (oldDataBillsec === billsec) {
                            setRows([])
                            if (billsec == undefined) {
                                clearInterval(interval)
                            }
                        } else clearInterval(interval)
                    }, 7000);
                } else {
                    setTimeout(() => {
                        setRows([])

                    }, 5000);
                }
                break;

            case "RINGING":
                const ringingInterval = setTimeout(() => {
                    if (oldDataBillsec === billsec) {
                        setTimeout(() => {
                            if (billsec === 0) {
                                setRows([])
                            } else clearTimeout(ringingInterval)

                        }, 10000)

                    } else if (billsec === 0) {
                        clearTimeout(ringingInterval)

                    }
                }, 6000);
                break;

            case "EARLY":
                const earlyInterval = setTimeout(() => {
                    if (billsec === 0) {
                        if (oldDataBillsec == undefined) {
                            clearTimeout(earlyInterval)
                        } else {
                            setTimeout(() => {
                                if (oldDataBillsec === billsec) {
                                    setRows([])
                                    clearTimeout(earlyInterval)
                                }
                            }, 7000);
                        }
                    }
                }, 3500);
                break;

            default:
                setRows([])
        }
    }

    const handleRestart = () => {
        axios.get('http://170.78.56.86:11000/users/restart/active/calls/', {
        }).then(res => {
            alert("Restart done!");
        }).catch(err => console.error(err));
    }
    useEffect(() => {
        const centrifuge = new Centrifuge('ws://170.78.56.48:3000/connection/websocket');
        centrifuge.subscribe("active-calls", function (msg) {
            if (!newPause.current) {
                if (Array.isArray(msg.data.calls)) {
                    setRows(msg.data.calls)
                    setTimeout(() => {
                        setOldBillsec([msg.data.calls[0]])
                    }, 8000)
                    if (rows.length === 0) {
                        setOldBillsec([])
                    }
                }
            }
        });
        centrifuge.on('error', function (context) {
            if (!context.reconnect) {
                setErrorWS(true);
            }
        })
        centrifuge.connect();

        return () => {
            centrifuge.removeAllListeners();
            centrifuge.disconnect();
        }
    }, [])


    const handleContextMenu = (event) => {
        event.preventDefault();
        if (event.target.dataset.context) {
            setContextMenuField(event.target.dataset.context);
            setContextMenu(
                contextMenu === null
                    ? {
                        mouseX: event.clientX - 2,
                        mouseY: event.clientY - 4,
                    } : null
                // repeated contextmenu when it is already open closes it with Chrome 84 on Ubuntu
                // Other native context menus might behave different.
                // With this behavior we prevent contextmenu from the backdrop to re-locale existing context menus.
            );
        }
    };

    const handleCloseError = () => {
        setErrorWS(false);
    };

    const handleClose = () => {
        setContextMenu(null);
    };

    const handlePause = () => {
        setPause(!pause);
        newPause.current = !newPause.current;
    }

    const handleScroll = (event) => {
        setTableScroll(closestMultiple(event.target.scrollTop, acRowSize));
    }

    const ownFiltering = (row) => {
        for (let f of filters) {
            if (!filter_ac(f, row)) {
                return false
            }
        }
        return true
    }


    const handleDeleteFiltersHeader = (field) => {
        setFilters(filters.filter(f => f["field"] !== field));
    }


    return (
        <div style={{ padding: 10 }}>
            <Box component={Paper} style={{ display: 'flex', padding: '0 8px', height: 32, flexWrap: 'nowrap', alignContent: 'flex-start', width: '100%', maxWidth: 'unset', borderRadius: 0 }}>
                <Stack
                    direction="row"
                    divider={<Divider orientation="vertical" flexItem />}
                    sx={{ py: 1, position: 'sticky', left: (isMobile ? 8 : 57 + 8) }}
                    spacing={1}
                >
                    <Box sx={{ fontSize: 10, display: 'inline-flex', justifyContent: 'center', alignItems: 'center', whiteSpace: 'nowrap' }}>Total Row Calls: {rows.length}</Box>
                    <Box sx={{ fontSize: 10, display: 'inline-flex', justifyContent: 'center', alignItems: 'center', whiteSpace: 'nowrap' }}>Connected Calls: {rows.filter(el => el["billsec"] > 0).length}</Box>
                    <Box sx={{ fontSize: 10, display: 'inline-flex', justifyContent: 'center', alignItems: 'center', whiteSpace: 'nowrap' }}>Filtered Calls: {filters.length === 0 ? 0 : rows.filter(ownFiltering).length}</Box>
                    <Box sx={{ fontSize: 10, display: 'inline-flex', justifyContent: 'center', alignItems: 'center', whiteSpace: 'nowrap' }}>Filtered Connected: {filters.length === 0 ? 0 : rows.filter(el => el["billsec"] > 0).filter(ownFiltering).length}</Box>

                    <Tooltip title="Clear Filters" placement="top">
                        <IconButton size="small" color="warning" disabled={!filters.length} onClick={() => setFilters([])}>
                            <FilterAltOffIcon sx={{ fontSize: 16 }} />
                        </IconButton>
                    </Tooltip>

                    <Tooltip title={pause ? "Start again active calls" : "Pause active calls"} placement="top">
                        <IconButton size="small" color="primary" disabled={!rows.length} onClick={handlePause}>
                            {pause ? <PlayCircleFilledIcon sx={{ fontSize: 16 }} /> : <PauseCircleIcon sx={{ fontSize: 16 }} />}
                        </IconButton>
                    </Tooltip>

                    <Tooltip title="Restart Active Calls" placement="top">
                        <IconButton size="small" color="secondary" onClick={handleRestart}>
                            <CachedIcon sx={{ fontSize: 16 }} />
                        </IconButton>
                    </Tooltip>
                </Stack>
            </Box>

            <Box component={Paper} style={{ display: 'flex', flexWrap: 'nowrap', alignContent: 'flex-start', width: '100%', borderRadius: 0, borderBottom: '2px solid #c5c5c5' }}>
                <MemoTableHeader filters={filters} handleDelete={handleDeleteFiltersHeader} />
            </Box>

            <Box onContextMenu={handleContextMenu} onScroll={(e) => handleScroll(e)} component="div" style={{ cursor: 'context-menu', display: 'flex', maxWidth: '100%', height: isMobile ? `calc(100vh - ${20 + 32 + 26 + 56 + acRowSize}px)` : (height - 20 - 32 - 26 - acRowSize), overflowY: 'auto', overflowX: 'hidden', boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)' }}>
                {rows.length === 0 ?
                    <Box component="div" sx={{ display: 'flex', flexWrap: 'wrap', alignContent: 'flex-start', width: 'auto', maxWidth: 'unset' }} style={{ height: isMobile ? `calc(100vh - ${20 + 32 + 26 + 56 + acRowSize}px)` : (height - 20 - 32 - 26 - acRowSize) }}>
                        {[...Array(Math.floor((height - 20 - 32 - 26 - acRowSize) / acRowSize))].map((el, idx) => <LoaderRow key={"loader-row--" + idx} />)}
                    </Box>
                    :
                    <Box component="div" sx={{ display: 'flex', flexWrap: 'wrap', alignContent: 'flex-start', width: 'auto', maxWidth: 'unset' }} style={{ height: (rows.filter(ownFiltering).length * acRowSize) }}>
                        <div style={{ height: tableScroll, width: '100%' }}></div>
                        {rows.sort((a, b) => a["billsec"] > b["billsec"] ? -1 : 1).filter(ownFiltering).filter((el, idx) => (((idx * acRowSize) > tableScroll - (acRowSize * 4)) && ((idx * acRowSize) < tableScroll + height + (4 * acRowSize)))).map((el) => <TableBodyRow key={`${el["uuid"]}--${el["ip_switch"]}`} {...el} />)}
                    </Box>
                }

            </Box>


            <FilterMenu
                filters={filters}
                setFilters={setFilters}
                contextMenuField={contextMenuField}
                handleContextMenu={handleContextMenu}
                handleClose={handleClose}
                contextMenu={contextMenu}
            />



            <Dialog
                open={errorWS}
                onClose={handleCloseError}
                aria-describedby="error-de-conexion"
            >
                <DialogTitle>Oops! we couldn't connect to the Active Calls</DialogTitle>
                <DialogContent>
                    <DialogContentText id="error-de-conexion">
                        Check your internet connection and reload the page, if this message keeps popping up contact support!
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button color="info" variant="contained" onClick={handleCloseError}>Ok</Button>
                </DialogActions>
            </Dialog>

        </div>
    )
}