import React from 'react';
import { Box } from '@mui/system';


type MyTableBodyCell = {
    field: string;
    width: number;
    value?: number | string;
    bgcolor?: string;
}


export const TableBodyCell = ({ field, width, value, bgcolor }: MyTableBodyCell) => {
    return (
        <Box
            component="div"
            data-context={field}
            sx={{ whiteSpace: 'nowrap', width: width, px: 2, boxSizing: 'content-box', bgcolor: bgcolor ? bgcolor : undefined }}>
            <span data-context={field}>{value ? value : "---"}</span>
        </Box>
    )
}
