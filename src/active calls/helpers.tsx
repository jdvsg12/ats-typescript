type ColumnData = DataColumn[];
type DataString = ColumnString;
type DataType = string;
type lookupDataString = lookupString;

export type ColumnTitleWidth = ColumnWidth;

interface DataColumn {
    verbose: string;
    field: string;
}
interface ColumnWidth {
    [key: string]: any;
    country: number;
    area_code: number;
    porta: number;
    src_number: number;
    dst_number: number;
    call_start: number;
    call_answered: number;
    state: number;
    billsec: number;
    media: number;
    customer: number;
    prefix_acl: number;
    provider: number;
    dst_prefix: number;
    dst_transformed: number;
    rate: number;
    pr_billed: number;
    cl_rate: number;
    cl_billed: number;
    profit: number;
    ip_real: number;
    ip_caller: number;
    ip_called: number;
    ip_switch: number;
    routing_by: number;
    src_rol: number;
    dst_rol: number;
    cause_sip: number;
    q850: number;
    acd: number;
    asr: number;
    sec: number;
    tat: number;
    tct: number;
    tft: number;
    tst: number;
    tcls: number;
    t_calls: number;
    t_fails: number;
    t_answer: number;
    t_fails_t: number;
    cause_sip_y: number;
    q850_y: number;
    acd_y: number;
    asr_y: number;
    sec_y: number;
    tat_y: number;
    tct_y: number;
    tft_y: number;
    tst_y: number;
    tcls_y: number;
    t_calls_y: number;
    t_fails_y: number;
    t_answer_y: number;
    t_fails_t_y: number;
    park_timeout: number;
    park_time: number;
    park_ininit: number;
    cid_name: number;
    cid_number: number;
    pai: number;
    rpid: number;
    did_transformation: number;
    codecs: number;
}
export interface ColumnString {
    [key: string]: any;
    country: string;
    area_code: string;
    porta: string;
    src_number: string;
    dst_number: string;
    call_start: string;
    call_answered: string;
    state: string;
    billsec: string;
    media: string;
    customer: string;
    prefix_acl: string;
    provider: string;
    dst_prefix: string;
    dst_transformed: string;
    rate: string;
    pr_billed: string;
    cl_rate: string;
    cl_billed: string;
    profit: string;
    ip_real: string;
    ip_caller: string;
    ip_called: string;
    ip_switch: string;
    routing_by: string;
    src_rol: string;
    dst_rol: string;
    cause_sip: string;
    q850: string;
    acd: string;
    asr: string;
    sec: string;
    tat: string;
    tct: string;
    tft: string;
    tst: string;
    tcls: string;
    t_calls: string;
    t_fails: string;
    t_answer: string;
    t_fails_t: string;
    cause_sip_y: string;
    q850_y: string;
    acd_y: string;
    asr_y: string;
    sec_y: string;
    tat_y: string;
    tct_y: string;
    tft_y: string;
    tst_y: string;
    tcls_y: string;
    t_calls_y: string;
    t_fails_y: string;
    t_answer_y: string;
    t_fails_t_y: string;
    park_timeout: string;
    park_time: string;
    park_ininit: string;
    cid_name: string;
    cid_number: string;
    pai: string;
    rpid: string;
    did_transformation: string;
    codecs: string;
}

interface lookupString {
    [key: string]: any
    "^a-b": string;
    "$a-b": string;
    "=a-b": string;
    "!a-b": string;
    "not ^": string;
    "not $": string;
    "not %": string;
    "not": string;
    "not len": string;
    ">= len": string;
    "<= len": string;
    "< len": string;
    "> len": string;
    "not in l1-l2": string;
    "in l1-l2": string;
    "len": string;
    "=": string;
    "<=": string;
    "<": string;
    ">=": string;
    ">": string;
    "^": string;
    "$": string;
    "[empty]": string;
    "[not empty]": string;
    "": string;
}
export interface RowColumnData {
    data: DataType;
    area_code: string;
    billsec: number;
    call_answered: string;
    call_start: string;
    cid_name: string;
    cid_number: string;
    cl_billed: number;
    cl_rate: string;
    codecs: string;
    country: string;
    customer: string;
    did_transformation: string;
    dst_number: string;
    dst_prefix: string;
    dst_rol: string;
    dst_rol_color: string;
    dst_transformed: string;
    id: string;
    initial_cid_name: string;
    initial_cid_number: string;
    initial_pai: string;
    initial_rpid: string;
    ip_called: string;
    ip_caller: string;
    ip_real: string;
    ip_switch: string;
    media: string;
    pai: string;
    park_ininit: string;
    park_time: number;
    park_timeout: string;
    porta: string;
    pr_billed: number;
    prefix_acl: string;
    profit: number;
    provider: string;
    rate: string;
    rol_data: string;
    rol_data_yesterday: string;
    routing_by: string;
    rpid: string;
    src_number: string;
    src_rol: string;
    src_rol_color: string;
    state: string;
    uuid: string;
}

export const columns: ColumnData = [
    { "verbose": "Location", "field": "country" },
    { "verbose": "Area Code", "field": "area_code" },
    { "verbose": "Porta", "field": "porta" },
    { "verbose": "SRC", "field": "src_number" },
    { "verbose": "DST IN", "field": "dst_number" },
    { "verbose": "Call Start T", "field": "call_start" },
    { "verbose": "Answered T", "field": "call_answered" },
    { "verbose": "State", "field": "state" },
    { "verbose": "Billsec", "field": "billsec" },
    { "verbose": "Media", "field": "media" },
    { "verbose": "Client", "field": "customer" },
    { "verbose": "Prefix DST IN", "field": "prefix_acl" },
    { "verbose": "Provider", "field": "provider" },
    { "verbose": "Prefix DST OUT", "field": "dst_prefix" },
    { "verbose": "DST OUT", "field": "dst_transformed" },
    { "verbose": "Rate PR", "field": "rate" },
    { "verbose": "Billed PR", "field": "pr_billed" },
    { "verbose": "Rate CL", "field": "cl_rate" },
    { "verbose": "Billed  CL", "field": "cl_billed" },
    { "verbose": "Profit", "field": "profit" },
    { "verbose": "IP Real", "field": "ip_real" },
    { "verbose": "IP Caller", "field": "ip_caller" },
    { "verbose": "IP Called", "field": "ip_called" },
    { "verbose": "Switch", "field": "ip_switch" },
    { "verbose": "Routing By", "field": "routing_by" },
    { "verbose": "Rol SRC", "field": "src_rol" },
    { "verbose": "Rol DST", "field": "dst_rol" },
    { "verbose": "Cause SIP", "field": "cause_sip" },
    { "verbose": "Q850", "field": "q850" },
    { "verbose": "ACD", "field": "acd" },
    { "verbose": "ASR", "field": "asr" },
    { "verbose": "Sec", "field": "sec" },
    { "verbose": "TAT", "field": "tat" },
    { "verbose": "TCT", "field": "tct" },
    { "verbose": "TFT", "field": "tft" },
    { "verbose": "TST", "field": "tst" },
    { "verbose": "TCLS", "field": "tcls" },
    { "verbose": "T Calls", "field": "t_calls" },
    { "verbose": "T Fails", "field": "t_fails" },
    { "verbose": "T Answer", "field": "t_answer" },
    { "verbose": "T Fails Temp", "field": "t_fails_t" },
    { "verbose": "Cause SIP Y", "field": "cause_sip_y" },
    { "verbose": "Q850 Y", "field": "q850_y" },
    { "verbose": "ACD Y", "field": "acd_y" },
    { "verbose": "ASR Y", "field": "asr_y" },
    { "verbose": "Sec Y", "field": "sec_y" },
    { "verbose": "TAT Y", "field": "tat_y" },
    { "verbose": "TCT Y", "field": "tct_y" },
    { "verbose": "TFT Y", "field": "tft_y" },
    { "verbose": "TST Y", "field": "tst_y" },
    { "verbose": "TCLS Y", "field": "tcls_y" },
    { "verbose": "T Calls Y", "field": "t_calls_y" },
    { "verbose": "T Fails Y", "field": "t_fails_y" },
    { "verbose": "T Answer Y", "field": "t_answer_y" },
    { "verbose": "T Fails Temp Y", "field": "t_fails_t_y" },
    { "verbose": "Park Timeout", "field": "park_timeout" },
    { "verbose": "Park Time", "field": "park_time" },
    { "verbose": "PARK IN Init", "field": "park_ininit" },
    { "verbose": "CID Name Sent", "field": "cid_name" },
    { "verbose": "CID Number Sent", "field": "cid_number" },
    { "verbose": "PAI", "field": "pai" },
    { "verbose": "RPID", "field": "rpid" },
    { "verbose": "DID Trans", "field": "did_transformation" },
    { "verbose": "Codecs", "field": "codecs" }
];

export const columns_verbose: DataString = {
    "country": "Location",
    "area_code": "Area Code",
    "porta": "Porta",
    "src_number": "SRC",
    "dst_number": "DST IN",
    "call_start": "Call Start T",
    "call_answered": "Answered T",
    "state": "State",
    "billsec": "Billsec",
    "media": "Media",
    "customer": "Client",
    "prefix_acl": "Prefix DST IN",
    "provider": "Provider",
    "dst_prefix": "Prefix DST OUT",
    "dst_transformed": "DST OUT",
    "rate": "Rate PR",
    "pr_billed": "Billed PR",
    "cl_rate": "Rate CL",
    "cl_billed": "Billed  CL",
    "profit": "Profit",
    "ip_real": "IP Real",
    "ip_caller": "IP Caller",
    "ip_called": "IP Called",
    "ip_switch": "Switch",
    "routing_by": "Routing By",
    "src_rol": "Rol SRC",
    "dst_rol": "Rol DST",
    "cause_sip": "Cause SIP",
    "q850": "Q850",
    "acd": "ACD",
    "asr": "ASR",
    "sec": "Sec",
    "tat": "TAT",
    "tct": "TCT",
    "tft": "TFT",
    "tst": "TST",
    "tcls": "TCLS",
    "t_calls": "T Calls",
    "t_fails": "T Fails",
    "t_answer": "T Answer",
    "t_fails_t": "T Fails Temp",
    "cause_sip_y": "Cause SIP Y",
    "q850_y": "Q850 Y",
    "acd_y": "ACD Y",
    "asr_y": "ASR Y",
    "sec_y": "Sec Y",
    "tat_y": "TAT Y",
    "tct_y": "TCT Y",
    "tft_y": "TFT Y",
    "tst_y": "TST Y",
    "tcls_y": "TCLS Y",
    "t_calls_y": "T Calls Y",
    "t_fails_y": "T Fails Y",
    "t_answer_y": "T Answer Y",
    "t_fails_t_y": "T Fails Temp Y",
    "park_timeout": "Park Timeout",
    "park_time": "Park Time",
    "park_ininit": "PARK IN Init",
    "cid_name": "CID Name Sent",
    "cid_number": "CID Number Sent",
    "pai": "PAI",
    "rpid": "RPID",
    "did_transformation": "DID Trans",
    "codecs": "Codecs"
};

export const columns_width: ColumnTitleWidth = {
    "country": 250,
    "area_code": 100,
    "porta": 100,
    "src_number": 100,
    "dst_number": 150,
    "call_start": 100,
    "call_answered": 100,
    "state": 60,
    "billsec": 60,
    "media": 100,
    "customer": 200,
    "prefix_acl": 100,
    "provider": 200,
    "dst_prefix": 100,
    "dst_transformed": 200,
    "rate": 100,
    "pr_billed": 100,
    "cl_rate": 100,
    "cl_billed": 100,
    "profit": 100,
    "ip_real": 100,
    "ip_caller": 100,
    "ip_called": 100,
    "ip_switch": 100,
    "routing_by": 100,
    "src_rol": 100,
    "dst_rol": 100,
    "cause_sip": 75,
    "q850": 75,
    "acd": 75,
    "asr": 75,
    "sec": 75,
    "tat": 75,
    "tct": 75,
    "tft": 75,
    "tst": 75,
    "tcls": 75,
    "t_calls": 75,
    "t_fails": 75,
    "t_answer": 75,
    "t_fails_t": 75,
    "cause_sip_y": 95,
    "q850_y": 95,
    "acd_y": 95,
    "asr_y": 95,
    "sec_y": 95,
    "tat_y": 95,
    "tct_y": 95,
    "tft_y": 95,
    "tst_y": 95,
    "tcls_y": 95,
    "t_calls_y": 95,
    "t_fails_y": 95,
    "t_answer_y": 95,
    "t_fails_t_y": 95,
    "park_timeout": 200,
    "park_time": 60,
    "park_ininit": 100,
    "cid_name": 200,
    "cid_number": 200,
    "pai": 100,
    "rpid": 100,
    "did_transformation": 100,
    "codecs": 760,
};

export const acRowSize: number = 17.5;

export function getWindowDimensions() {
    const { innerWidth: width, innerHeight: height } = window;
    return {
        width,
        height
    };
}

export function closestMultiple(scrollDownNumber: number, boxHeightfixed: number) {
    if (boxHeightfixed > scrollDownNumber)
        return 0;
    const num = Math.floor(boxHeightfixed / 2)
    scrollDownNumber = scrollDownNumber + num;
    scrollDownNumber = scrollDownNumber - (scrollDownNumber % boxHeightfixed);
    return scrollDownNumber;
}

export const lookups = [
    "^a-b",
    "$a-b",
    "=a-b",
    "!a-b",
    "not ^",
    "not $",
    "not %",
    "not",
    "not len",
    ">= len",
    "<= len",
    "< len",
    "> len",
    "not in l1-l2",
    "in l1-l2",
    "len",
    "=",
    "<=",
    "<",
    ">=",
    ">",
    "^",
    "$",
    "[empty]",
    "[not empty]"
];

export const lookups_verbose:lookupDataString = {
    "^a-b": "starts in range",
    "$a-b": "ends in range",
    "=a-b": "in range",
    "!a-b": "not in range",
    "not ^": "not starts",
    "not $": "not ends",
    "not %": "not contains",
    "not": "not equal",
    "not len": "not length",
    ">= len": ">= length",
    "<= len": "<= length",
    "< len": "< length",
    "> len": "> length",
    "not in l1-l2": "not in range l1-l2",
    "in l1-l2": "in length",
    "len": "length equal",
    "=": "=",
    "<=": "<=",
    "<": "<",
    ">=": ">=",
    ">": ">",
    "^": "starts",
    "$": "ends",
    "[empty]": "is empty",
    "[not empty]": "is not empty",
    "": "contains"
};

function reverse(str: string) {
    return str.split("").reverse().join("");
}

const numericColumns = [
    "rate",
    "pr_billed",
    "cl_rate",
    "cl_billed",
    "profit"
];

const dateColumns = [
    "billsec",
    "call_start",
    "call_answered"
]

const dataStatsColumns = [
    "acd",
    "asr",
    "sec",
    "tat",
    "tct",
    "tft",
    "tst",
    "tcls",
    "t_calls",
    "t_fails",
    "t_answer",
    "t_fails_t"
]

const dataStatsOuterColumns = [
    "cause_sip",
    "q850"
]

const dataStatsYesterdayColumns = [
    "acd_y",
    "asr_y",
    "sec_y",
    "tat_y",
    "tct_y",
    "tft_y",
    "tst_y",
    "tcls_y",
    "t_calls_y",
    "t_fails_y",
    "t_answer_y",
    "t_fails_t_y"
]

const dataStatsYesterdayOuterColumns = [
    "cause_sip_y",
    "q850_y"
]
export function filter_ac(filter: { field: string; lookup: string; value: string }, row: any) {
    const { field, lookup, value } = filter;
    let value_to_test;

    if (numericColumns.includes(field)) {
        value_to_test = row[field].toString();
    } else if (dateColumns.includes(field)) {
        value_to_test = convertHHMMSS(row[field]);
    } else if (dataStatsColumns.includes(field)) {
        if (isValidJSON(row["rol_data"])) {
            const data = JSON.parse(row["rol_data"]);
            if ("stats" in data) {
                value_to_test = field in data["stats"] ? data["stats"][field].toString() : "";
            } else {
                value_to_test = "";
            }
        } else {
            value_to_test = "";
        }
    } else if (dataStatsOuterColumns.includes(field)) {
        if (isValidJSON(row["rol_data"])) {
            const data = JSON.parse(row["rol_data"]);
            value_to_test = field in data ? data[field] : "";
        } else {
            value_to_test = "";
        }

    } else if (dataStatsYesterdayColumns.includes(field)) {
        if (isValidJSON(row["rol_data_yesterday"])) {
            const data = JSON.parse(row["rol_data_yesterday"]);

            if ("stats" in data) {
                value_to_test = field.replace("_y", "") in data["stats"] ? data["stats"][field.replace("_y", "")].toString() : "";
            } else {
                value_to_test = "";
            }
        } else {
            value_to_test = "";
        }

    } else if (dataStatsYesterdayOuterColumns.includes(field)) {
        if (isValidJSON(row["rol_data_yesterday"])) {
            const data = JSON.parse(row["rol_data_yesterday"]);
            value_to_test = field.replace("_y", "") in data ? data[field.replace("_y", "")] : "";
        } else {
            value_to_test = "";
        }
    } else {
        value_to_test = row[field].toLowerCase();
    }

    let splitted = [];

    switch (lookup) {

        case "^a-b":
            splitted = value.split("-")
            if (splitted.length === 2) {
                splitted = splitted[0] <= splitted[1] ? [splitted[0], splitted[1]] : [splitted[1], splitted[0]];

                if (value_to_test.substring(0, splitted[0].length) >= splitted[0] && value_to_test.substring(0, splitted[1].length) <= splitted[1]) {
                    return true;
                }
            }
            return false;

        case "$a-b":

            splitted = value.split("-")
            if (splitted.length === 2) {
                splitted = splitted[0] <= splitted[1] ? [splitted[0], splitted[1]] : [splitted[1], splitted[0]];

                if (reverse(reverse(value_to_test).substring(0, splitted[0].length)) >= splitted[0] && reverse(reverse(value_to_test).substring(0, splitted[1].length)) <= splitted[1]) {
                    return true;
                }
            }
            return false;

        case "=a-b":

            splitted = value.split("-")
            if (splitted.length === 2) {
                splitted = splitted[0] <= splitted[1] ? [splitted[0], splitted[1]] : [splitted[1], splitted[0]];

                if (value_to_test >= splitted[0] && value_to_test <= splitted[1]) {
                    return true;
                }
            }
            return false;

        case "!a-b":

            splitted = value.split("-")
            if (splitted.length === 2) {
                splitted = splitted[0] <= splitted[1] ? [splitted[0], splitted[1]] : [splitted[1], splitted[0]];

                if (value_to_test < splitted[0] || value_to_test > splitted[1]) {
                    return true;
                }
            }
            return false;

        case "not ^":

            return !value_to_test.startsWith(value);

        case "not $":

            return !value_to_test.endsWith(value);

        case "not %":

            return !value_to_test.includes(value);

        case "not":

            return value_to_test !== value;

        case "len":

            return value_to_test.length === parseInt(value);

        case "not len":

            return value_to_test.length !== parseInt(value);

        case ">= len":

            return value_to_test.length >= parseInt(value);

        case "<= len":

            return value_to_test.length <= parseInt(value);

        case "< len":

            return value_to_test.length < parseInt(value);

        case "> len":

            return value_to_test.length > parseInt(value);

        case "in l1-l2":

            splitted = value.split("-")
            if (splitted.length === 2) {
                splitted = splitted[0] <= splitted[1] ? [splitted[0], splitted[1]] : [splitted[1], splitted[0]];

                return value_to_test.length >= parseInt(splitted[0]) && value_to_test.length <= parseInt(splitted[1]);
            }
            return false;

        case "not in l1-l2":


            splitted = value.split("-")
            if (splitted.length === 2) {
                splitted = splitted[0] <= splitted[1] ? [splitted[0], splitted[1]] : [splitted[1], splitted[0]];
                return value_to_test.length < parseInt(splitted[0]) || value_to_test.length > parseInt(splitted[1]);
            }
            return false;

        case "=":

            return value_to_test === value;

        case "<=":

            return value_to_test <= value;

        case "<":

            return value_to_test < value;

        case ">=":

            return value_to_test >= value;

        case ">":

            return value_to_test > value;

        case "^":

            return value_to_test.startsWith(value);

        case "$":

            return value_to_test.endsWith(value);

        case "[empty]":
            return !value_to_test

        case "[not empty]":
            return !!value_to_test

        default:
            return value_to_test.includes(value);

    }
}


export function convertHHMMSS(value: any) {
    const sec = parseInt(value, 10); // convert value to number if it's string
    let hours: any   = Math.floor(sec / 3600); // get hours
    let minutes: any = Math.floor((sec - (hours * 3600)) / 60); // get minutes
    let seconds: any = sec - (hours * 3600) - (minutes * 60); //  get seconds
    // add 0 if value < 10; Example: 2 => 02
    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds; // Return is HH : MM : SS
}

export function isValidJSON(obj: any) {
    try {
        JSON.parse(obj);
        return true;
    } catch {
        return false;
    }
}