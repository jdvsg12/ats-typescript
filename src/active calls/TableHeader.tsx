import React from 'react';
import { Box } from '@mui/system';
import { columns, columns_width } from "./helpers";
import { Divider, IconButton } from '@mui/material';
import FilterAltOffIcon from '@mui/icons-material/FilterAltOff';

type FilterHeaderCellType = {
    field: string;
    value: string;
    lookup: string;
}

interface TableHeaderCellProps {
    filters: FilterHeaderCellType[];
    handleDelete: any;
    field: string;
    verbose: string;
    filtersCheck?: boolean;
}

const TableHeader = ({ filters, handleDelete }: TableHeaderCellProps) => {
    return (
        <React.Fragment>
            {columns.map((ColumnsProps) => {
                const filtersCheck = filters.some(f => f["field"] === ColumnsProps.field)
                const widthData = columns_width[ColumnsProps["field"]]
                return (
                    <>
                        <Box
                            key={`${ColumnsProps.field}--header`}
                            sx={{ margin: '0 16px', display: 'flex', alignItems: 'center', whiteSpace: 'nowrap', justifyContent: 'space-between', width: widthData, height: 24, fontSize: 12, fontWeight: 'bold' }}>
                            {ColumnsProps["verbose"]}
                            {filtersCheck ?
                                <IconButton size="small" color="primary" onClick={() => handleDelete(ColumnsProps.field)}>
                                    <FilterAltOffIcon style={{ fontSize: 12 }} />
                                </IconButton> : null}
                        </Box>
                        {ColumnsProps.field === "codecs" ? null : <Divider orientation="vertical" variant="middle" flexItem />}
                    </>
                )
            })}
        </React.Fragment>
    )
}

const MemoTableHeader = React.memo(TableHeader)


export default MemoTableHeader;